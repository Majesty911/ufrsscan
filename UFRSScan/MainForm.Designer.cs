﻿namespace UFRSScan
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.View_groupBox_Scan = new System.Windows.Forms.GroupBox();
            this.gBoxDebug = new System.Windows.Forms.GroupBox();
            this.cBoxDeskew = new System.Windows.Forms.CheckBox();
            this.cBoxAutoFeed = new System.Windows.Forms.CheckBox();
            this.cBoxNative = new System.Windows.Forms.CheckBox();
            this.cBoxAutoBorder = new System.Windows.Forms.CheckBox();
            this.cBoxAutoSence = new System.Windows.Forms.CheckBox();
            this.checkBox_HideInterface = new System.Windows.Forms.CheckBox();
            this.groupBox_Duplex = new System.Windows.Forms.GroupBox();
            this.radioButton_Duplex_off = new System.Windows.Forms.RadioButton();
            this.radioButton_Duplex_on = new System.Windows.Forms.RadioButton();
            this.groupBox_Format = new System.Windows.Forms.GroupBox();
            this.comboBox_Format = new System.Windows.Forms.ComboBox();
            this.groupBox_DPI = new System.Windows.Forms.GroupBox();
            this.View_comboBox_DPI = new System.Windows.Forms.ComboBox();
            this.View_groupBox_color = new System.Windows.Forms.GroupBox();
            this.view_radioButton_color = new System.Windows.Forms.RadioButton();
            this.view_radioButton_BW = new System.Windows.Forms.RadioButton();
            this.View_pictureBox_CurrentImg = new System.Windows.Forms.PictureBox();
            this.View_menuStrip = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.sepSourceList = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemReload = new System.Windows.Forms.ToolStripMenuItem();
            this.View_ToolStripMenuItem_runScaning = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView_ScanImages = new System.Windows.Forms.DataGridView();
            this.View_groupBox_Scan.SuspendLayout();
            this.gBoxDebug.SuspendLayout();
            this.groupBox_Duplex.SuspendLayout();
            this.groupBox_Format.SuspendLayout();
            this.groupBox_DPI.SuspendLayout();
            this.View_groupBox_color.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.View_pictureBox_CurrentImg)).BeginInit();
            this.View_menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ScanImages)).BeginInit();
            this.SuspendLayout();
            // 
            // View_groupBox_Scan
            // 
            this.View_groupBox_Scan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.View_groupBox_Scan.Controls.Add(this.gBoxDebug);
            this.View_groupBox_Scan.Controls.Add(this.checkBox_HideInterface);
            this.View_groupBox_Scan.Controls.Add(this.groupBox_Duplex);
            this.View_groupBox_Scan.Controls.Add(this.groupBox_Format);
            this.View_groupBox_Scan.Controls.Add(this.groupBox_DPI);
            this.View_groupBox_Scan.Controls.Add(this.View_groupBox_color);
            this.View_groupBox_Scan.Location = new System.Drawing.Point(13, 27);
            this.View_groupBox_Scan.Name = "View_groupBox_Scan";
            this.View_groupBox_Scan.Size = new System.Drawing.Size(186, 443);
            this.View_groupBox_Scan.TabIndex = 0;
            this.View_groupBox_Scan.TabStop = false;
            // 
            // gBoxDebug
            // 
            this.gBoxDebug.Controls.Add(this.cBoxDeskew);
            this.gBoxDebug.Controls.Add(this.cBoxAutoFeed);
            this.gBoxDebug.Controls.Add(this.cBoxNative);
            this.gBoxDebug.Controls.Add(this.cBoxAutoBorder);
            this.gBoxDebug.Controls.Add(this.cBoxAutoSence);
            this.gBoxDebug.Location = new System.Drawing.Point(7, 311);
            this.gBoxDebug.Name = "gBoxDebug";
            this.gBoxDebug.Size = new System.Drawing.Size(172, 126);
            this.gBoxDebug.TabIndex = 6;
            this.gBoxDebug.TabStop = false;
            this.gBoxDebug.Text = "Debug";
            // 
            // cBoxDeskew
            // 
            this.cBoxDeskew.AutoSize = true;
            this.cBoxDeskew.Location = new System.Drawing.Point(6, 109);
            this.cBoxDeskew.Name = "cBoxDeskew";
            this.cBoxDeskew.Size = new System.Drawing.Size(87, 17);
            this.cBoxDeskew.TabIndex = 10;
            this.cBoxDeskew.Text = "AutoDeskew";
            this.cBoxDeskew.UseVisualStyleBackColor = true;
            // 
            // cBoxAutoFeed
            // 
            this.cBoxAutoFeed.AutoSize = true;
            this.cBoxAutoFeed.Location = new System.Drawing.Point(6, 65);
            this.cBoxAutoFeed.Name = "cBoxAutoFeed";
            this.cBoxAutoFeed.Size = new System.Drawing.Size(72, 17);
            this.cBoxAutoFeed.TabIndex = 7;
            this.cBoxAutoFeed.Text = "AutoFeed";
            this.cBoxAutoFeed.UseVisualStyleBackColor = true;
            // 
            // cBoxNative
            // 
            this.cBoxNative.AutoSize = true;
            this.cBoxNative.Location = new System.Drawing.Point(6, 88);
            this.cBoxNative.Name = "cBoxNative";
            this.cBoxNative.Size = new System.Drawing.Size(57, 17);
            this.cBoxNative.TabIndex = 6;
            this.cBoxNative.Text = "Native";
            this.cBoxNative.UseVisualStyleBackColor = true;
            // 
            // cBoxAutoBorder
            // 
            this.cBoxAutoBorder.AutoSize = true;
            this.cBoxAutoBorder.Location = new System.Drawing.Point(6, 42);
            this.cBoxAutoBorder.Name = "cBoxAutoBorder";
            this.cBoxAutoBorder.Size = new System.Drawing.Size(79, 17);
            this.cBoxAutoBorder.TabIndex = 9;
            this.cBoxAutoBorder.Text = "AutoBorder";
            this.cBoxAutoBorder.UseVisualStyleBackColor = true;
            // 
            // cBoxAutoSence
            // 
            this.cBoxAutoSence.AutoSize = true;
            this.cBoxAutoSence.Location = new System.Drawing.Point(6, 19);
            this.cBoxAutoSence.Name = "cBoxAutoSence";
            this.cBoxAutoSence.Size = new System.Drawing.Size(79, 17);
            this.cBoxAutoSence.TabIndex = 8;
            this.cBoxAutoSence.Text = "AutoSence";
            this.cBoxAutoSence.UseVisualStyleBackColor = true;
            // 
            // checkBox_HideInterface
            // 
            this.checkBox_HideInterface.AutoSize = true;
            this.checkBox_HideInterface.Location = new System.Drawing.Point(12, 287);
            this.checkBox_HideInterface.Name = "checkBox_HideInterface";
            this.checkBox_HideInterface.Size = new System.Drawing.Size(167, 17);
            this.checkBox_HideInterface.TabIndex = 5;
            this.checkBox_HideInterface.Text = "Скрыть интерфейс сканера";
            this.checkBox_HideInterface.UseVisualStyleBackColor = true;
            // 
            // groupBox_Duplex
            // 
            this.groupBox_Duplex.Controls.Add(this.radioButton_Duplex_off);
            this.groupBox_Duplex.Controls.Add(this.radioButton_Duplex_on);
            this.groupBox_Duplex.Location = new System.Drawing.Point(6, 213);
            this.groupBox_Duplex.Name = "groupBox_Duplex";
            this.groupBox_Duplex.Size = new System.Drawing.Size(174, 68);
            this.groupBox_Duplex.TabIndex = 4;
            this.groupBox_Duplex.TabStop = false;
            this.groupBox_Duplex.Text = "Двустороннее сканирование";
            // 
            // radioButton_Duplex_off
            // 
            this.radioButton_Duplex_off.AutoSize = true;
            this.radioButton_Duplex_off.Location = new System.Drawing.Point(6, 42);
            this.radioButton_Duplex_off.Name = "radioButton_Duplex_off";
            this.radioButton_Duplex_off.Size = new System.Drawing.Size(55, 17);
            this.radioButton_Duplex_off.TabIndex = 1;
            this.radioButton_Duplex_off.TabStop = true;
            this.radioButton_Duplex_off.Text = "Выкл.";
            this.radioButton_Duplex_off.UseVisualStyleBackColor = true;
            // 
            // radioButton_Duplex_on
            // 
            this.radioButton_Duplex_on.AutoSize = true;
            this.radioButton_Duplex_on.Location = new System.Drawing.Point(6, 19);
            this.radioButton_Duplex_on.Name = "radioButton_Duplex_on";
            this.radioButton_Duplex_on.Size = new System.Drawing.Size(47, 17);
            this.radioButton_Duplex_on.TabIndex = 0;
            this.radioButton_Duplex_on.TabStop = true;
            this.radioButton_Duplex_on.Text = "Вкл.";
            this.radioButton_Duplex_on.UseVisualStyleBackColor = true;
            // 
            // groupBox_Format
            // 
            this.groupBox_Format.Controls.Add(this.comboBox_Format);
            this.groupBox_Format.Location = new System.Drawing.Point(6, 152);
            this.groupBox_Format.Name = "groupBox_Format";
            this.groupBox_Format.Size = new System.Drawing.Size(174, 55);
            this.groupBox_Format.TabIndex = 3;
            this.groupBox_Format.TabStop = false;
            this.groupBox_Format.Text = "Формат";
            // 
            // comboBox_Format
            // 
            this.comboBox_Format.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Format.FormattingEnabled = true;
            this.comboBox_Format.Location = new System.Drawing.Point(7, 20);
            this.comboBox_Format.Name = "comboBox_Format";
            this.comboBox_Format.Size = new System.Drawing.Size(161, 21);
            this.comboBox_Format.TabIndex = 0;
            // 
            // groupBox_DPI
            // 
            this.groupBox_DPI.Controls.Add(this.View_comboBox_DPI);
            this.groupBox_DPI.Location = new System.Drawing.Point(6, 19);
            this.groupBox_DPI.Name = "groupBox_DPI";
            this.groupBox_DPI.Size = new System.Drawing.Size(174, 44);
            this.groupBox_DPI.TabIndex = 2;
            this.groupBox_DPI.TabStop = false;
            this.groupBox_DPI.Text = "DPI";
            // 
            // View_comboBox_DPI
            // 
            this.View_comboBox_DPI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.View_comboBox_DPI.FormattingEnabled = true;
            this.View_comboBox_DPI.Location = new System.Drawing.Point(7, 17);
            this.View_comboBox_DPI.Name = "View_comboBox_DPI";
            this.View_comboBox_DPI.Size = new System.Drawing.Size(161, 21);
            this.View_comboBox_DPI.TabIndex = 0;
            this.View_comboBox_DPI.SelectedIndexChanged += new System.EventHandler(this.View_comboBox_DPI_SelectedIndexChanged);
            // 
            // View_groupBox_color
            // 
            this.View_groupBox_color.Controls.Add(this.view_radioButton_color);
            this.View_groupBox_color.Controls.Add(this.view_radioButton_BW);
            this.View_groupBox_color.Location = new System.Drawing.Point(6, 70);
            this.View_groupBox_color.Name = "View_groupBox_color";
            this.View_groupBox_color.Size = new System.Drawing.Size(174, 75);
            this.View_groupBox_color.TabIndex = 1;
            this.View_groupBox_color.TabStop = false;
            this.View_groupBox_color.Text = "Глубина";
            // 
            // view_radioButton_color
            // 
            this.view_radioButton_color.AutoSize = true;
            this.view_radioButton_color.Location = new System.Drawing.Point(7, 44);
            this.view_radioButton_color.Name = "view_radioButton_color";
            this.view_radioButton_color.Size = new System.Drawing.Size(68, 17);
            this.view_radioButton_color.TabIndex = 1;
            this.view_radioButton_color.Text = "Цветной";
            this.view_radioButton_color.UseVisualStyleBackColor = true;
            // 
            // view_radioButton_BW
            // 
            this.view_radioButton_BW.AutoSize = true;
            this.view_radioButton_BW.Checked = true;
            this.view_radioButton_BW.Location = new System.Drawing.Point(7, 20);
            this.view_radioButton_BW.Name = "view_radioButton_BW";
            this.view_radioButton_BW.Size = new System.Drawing.Size(92, 17);
            this.view_radioButton_BW.TabIndex = 0;
            this.view_radioButton_BW.TabStop = true;
            this.view_radioButton_BW.Text = "Черно-белый";
            this.view_radioButton_BW.UseVisualStyleBackColor = true;
            // 
            // View_pictureBox_CurrentImg
            // 
            this.View_pictureBox_CurrentImg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.View_pictureBox_CurrentImg.Location = new System.Drawing.Point(205, 27);
            this.View_pictureBox_CurrentImg.Name = "View_pictureBox_CurrentImg";
            this.View_pictureBox_CurrentImg.Size = new System.Drawing.Size(299, 443);
            this.View_pictureBox_CurrentImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.View_pictureBox_CurrentImg.TabIndex = 1;
            this.View_pictureBox_CurrentImg.TabStop = false;
            // 
            // View_menuStrip
            // 
            this.View_menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemSettings,
            this.View_ToolStripMenuItem_runScaning});
            this.View_menuStrip.Location = new System.Drawing.Point(0, 0);
            this.View_menuStrip.Name = "View_menuStrip";
            this.View_menuStrip.Size = new System.Drawing.Size(615, 24);
            this.View_menuStrip.TabIndex = 2;
            this.View_menuStrip.Text = "menuStrip1";
            // 
            // ToolStripMenuItemSettings
            // 
            this.ToolStripMenuItemSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sepSourceList,
            this.toolStripMenuItemReload});
            this.ToolStripMenuItemSettings.Name = "ToolStripMenuItemSettings";
            this.ToolStripMenuItemSettings.Size = new System.Drawing.Size(107, 20);
            this.ToolStripMenuItemSettings.Text = "Выбрать сканер";
            this.ToolStripMenuItemSettings.Click += new System.EventHandler(this.ToolStripMenuItem1_Click);
            // 
            // sepSourceList
            // 
            this.sepSourceList.Name = "sepSourceList";
            this.sepSourceList.Size = new System.Drawing.Size(125, 6);
            // 
            // toolStripMenuItemReload
            // 
            this.toolStripMenuItemReload.Name = "toolStripMenuItemReload";
            this.toolStripMenuItemReload.Size = new System.Drawing.Size(128, 22);
            this.toolStripMenuItemReload.Text = "Обновить";
            this.toolStripMenuItemReload.Click += new System.EventHandler(this.toolStripMenuItemReload_Click);
            // 
            // View_ToolStripMenuItem_runScaning
            // 
            this.View_ToolStripMenuItem_runScaning.Name = "View_ToolStripMenuItem_runScaning";
            this.View_ToolStripMenuItem_runScaning.Size = new System.Drawing.Size(90, 20);
            this.View_ToolStripMenuItem_runScaning.Text = "Сканировать";
            this.View_ToolStripMenuItem_runScaning.Click += new System.EventHandler(this.View_ToolStripMenuItem_runScaning_Click);
            // 
            // dataGridView_ScanImages
            // 
            this.dataGridView_ScanImages.AllowUserToAddRows = false;
            this.dataGridView_ScanImages.AllowUserToDeleteRows = false;
            this.dataGridView_ScanImages.AllowUserToResizeColumns = false;
            this.dataGridView_ScanImages.AllowUserToResizeRows = false;
            this.dataGridView_ScanImages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_ScanImages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dataGridView_ScanImages.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.dataGridView_ScanImages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_ScanImages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ScanImages.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = "null";
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_ScanImages.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_ScanImages.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView_ScanImages.Location = new System.Drawing.Point(520, 28);
            this.dataGridView_ScanImages.MultiSelect = false;
            this.dataGridView_ScanImages.Name = "dataGridView_ScanImages";
            this.dataGridView_ScanImages.RowHeadersVisible = false;
            this.dataGridView_ScanImages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView_ScanImages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_ScanImages.Size = new System.Drawing.Size(95, 442);
            this.dataGridView_ScanImages.TabIndex = 3;
            this.dataGridView_ScanImages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ScanImages_CellClick);
            this.dataGridView_ScanImages.SelectionChanged += new System.EventHandler(this.dataGridView_ScanImages_SelectionChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 482);
            this.Controls.Add(this.dataGridView_ScanImages);
            this.Controls.Add(this.View_pictureBox_CurrentImg);
            this.Controls.Add(this.View_groupBox_Scan);
            this.Controls.Add(this.View_menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.View_menuStrip;
            this.MinimumSize = new System.Drawing.Size(631, 458);
            this.Name = "MainForm";
            this.Text = "UFRSScan";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.View_groupBox_Scan.ResumeLayout(false);
            this.View_groupBox_Scan.PerformLayout();
            this.gBoxDebug.ResumeLayout(false);
            this.gBoxDebug.PerformLayout();
            this.groupBox_Duplex.ResumeLayout(false);
            this.groupBox_Duplex.PerformLayout();
            this.groupBox_Format.ResumeLayout(false);
            this.groupBox_DPI.ResumeLayout(false);
            this.View_groupBox_color.ResumeLayout(false);
            this.View_groupBox_color.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.View_pictureBox_CurrentImg)).EndInit();
            this.View_menuStrip.ResumeLayout(false);
            this.View_menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ScanImages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox View_groupBox_Scan;
        private System.Windows.Forms.PictureBox View_pictureBox_CurrentImg;
        private System.Windows.Forms.MenuStrip View_menuStrip;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemSettings;
        private System.Windows.Forms.ToolStripMenuItem View_ToolStripMenuItem_runScaning;
        private System.Windows.Forms.ComboBox View_comboBox_DPI;
        private System.Windows.Forms.GroupBox View_groupBox_color;
        private System.Windows.Forms.RadioButton view_radioButton_color;
        private System.Windows.Forms.RadioButton view_radioButton_BW;
        private System.Windows.Forms.DataGridView dataGridView_ScanImages;
        private System.Windows.Forms.GroupBox groupBox_Format;
        private System.Windows.Forms.ComboBox comboBox_Format;
        private System.Windows.Forms.GroupBox groupBox_DPI;
        private System.Windows.Forms.ToolStripSeparator sepSourceList;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReload;
        private System.Windows.Forms.CheckBox checkBox_HideInterface;
        private System.Windows.Forms.GroupBox groupBox_Duplex;
        private System.Windows.Forms.RadioButton radioButton_Duplex_off;
        private System.Windows.Forms.RadioButton radioButton_Duplex_on;
        private System.Windows.Forms.CheckBox cBoxNative;
        private System.Windows.Forms.CheckBox cBoxAutoFeed;
        private System.Windows.Forms.CheckBox cBoxAutoSence;
        private System.Windows.Forms.CheckBox cBoxAutoBorder;
        private System.Windows.Forms.GroupBox gBoxDebug;
        private System.Windows.Forms.CheckBox cBoxDeskew;
    }
}

