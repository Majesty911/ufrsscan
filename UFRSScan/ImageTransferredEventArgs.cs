﻿using System;
using System.Drawing;

namespace UFRSScan
{
    internal class ImageTransferredEventArgs : EventArgs
    {
        public Image ImageData { get; }

        public ImageTransferredEventArgs(Image imageImageData) : base()
        {
            ImageData = imageImageData;
        }
    }
}
