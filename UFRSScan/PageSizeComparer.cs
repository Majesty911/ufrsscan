﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTwain.Data;

namespace UFRSScan
{
    internal class PageSizeComparer : IComparer<SupportedSize>
    {
        public int Compare(SupportedSize a, SupportedSize b)
        {
            if (a == b)
                return 0;
            if (a == SupportedSize.None)
                return -1;
            if (b == SupportedSize.None)
                return 1;
            if (a.ToString().StartsWith("A"))
                return -1;
            if (b.ToString().StartsWith("A"))
                return 1;
            return string.CompareOrdinal(a.ToString(), b.ToString());
        }
    }
}
