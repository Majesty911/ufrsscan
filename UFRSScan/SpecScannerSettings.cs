﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTwain;
using NTwain.Data;

namespace UFRSScan
{
    static class SpecScannerSettings
    {
        private static readonly Dictionary<string, Action<Capabilities>> ScanDelegate = new Dictionary<string, Action<Capabilities>>()
        {
            {"Kyocera FS-1016MFP Series Scanne", cap =>
            {
                if(cap.ICapPixelType.GetCurrent() != PixelType.BlackWhite) return;
                var defFlavor = cap.ICapPixelFlavor.GetDefault();
                var curFlavor = cap.ICapPixelFlavor.GetCurrent();
                if (defFlavor != curFlavor) return;
                cap.ICapPixelFlavor.SetValue(curFlavor == PixelFlavor.Chocolate
                    ? PixelFlavor.Vanilla
                    : PixelFlavor.Chocolate);
            }}
        };

        public static Action<Capabilities> DefaultSettings { get; set; } =
            cap =>
            {
                if (cap.CapAutoFeed.CanSet && cap.CapAutoFeed.GetCurrent() != BoolType.True)
                    cap.CapAutoFeed.SetValue(BoolType.True);

                if (cap.CapAutomaticSenseMedium.CanSet && cap.CapAutomaticSenseMedium.GetCurrent() != BoolType.True)
                    cap.CapAutomaticSenseMedium.SetValue(BoolType.True);

                if (cap.ICapAutomaticBorderDetection.CanSet && cap.ICapAutomaticBorderDetection.GetCurrent() != BoolType.True)
                    cap.ICapAutomaticBorderDetection.SetValue(BoolType.True);

                if (cap.ICapAutomaticDeskew.CanSet && cap.ICapAutomaticDeskew.GetCurrent() != BoolType.True)
                    cap.ICapAutomaticDeskew.SetValue(BoolType.True);
            };

        public static bool Exist(string scannerName)
        {
            return ScanDelegate.ContainsKey(scannerName);
        }

        public static void Apply(string scannerName, Capabilities cap)
        {
            if(ScanDelegate.ContainsKey(scannerName))
                ScanDelegate[scannerName].Invoke(cap);
            DefaultSettings.Invoke(cap);
        }
    }
}
