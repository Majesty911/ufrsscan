﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace UFRSScan
{
    public class ScannedImage
    {
        [Browsable(false)]
        public int Count { get; }

        [Browsable(false)]
        public string PathToImg { get; }

        //[Browsable(false)]
        //public Image Preview { get; }

        public Image Thumbnail { get; }

        public ScannedImage(Image thumbnail, int count, string path)
        {
            Count = count;
            Thumbnail = thumbnail;
            PathToImg = path;
        }

        //public Image GetImage()
        //{
        //    if (!File.Exists(PathToImg)) return null;
        //    try
        //    {
        //        using (var readStream = new FileStream(PathToImg, FileMode.Open))
        //        {
        //            return new Bitmap(readStream);
        //        }
        //    }
        //    catch (Exception ex) { throw new Exception(string.Format("Не удалось прочитать изображение {0} : {1}", PathToImg, ex.Message), ex); }
        //}

        public Task<Bitmap> GetImageAsync()
        {
            if (!File.Exists(PathToImg)) return null;
            try
            {
                //return Task<Image>.Run(() => Image.FromFile(PathToImg));
                //return Task<Bitmap>.Run(() =>
                return TaskEx.Run(() =>
                {
                    using (var readStream = new FileStream(PathToImg, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize:4096, useAsync:true))
                    {
                        return new Bitmap(readStream);
                    }
                });
            }
            catch (Exception ex) { throw new Exception(string.Format("Не удалось прочитать изображение {0} : {1}", PathToImg, ex.Message), ex); }
        }
    }
}
