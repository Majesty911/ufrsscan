﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NTwain;
using NTwain.Data;

namespace UFRSScan  
{
    internal class TwainSimple : IEnumerable<DataSource>
    {
        private readonly TwainSession _session;

        public bool ckUIControl { get; private set; }
        public bool ckDPI { get; private set; }
        public bool ckPixelType { get; private set; }
        public bool ckDuplex { get; private set; }
        public bool ckSize { get; private set; }

        public int State => _session.State;
        public DataSource CurrentSource => _session.CurrentSource;

        public EventHandler SourceDisabled { get; set; }
        public EventHandler SourceChanged { get; set; }

        public TWFix32? DPI
        {
            get
            {
                if (!ckDPI || _session.State != (int) NTwain.State.SourceOpened)
                    return null;
                return _session.CurrentSource.Capabilities.ICapXResolution.GetCurrent();
            }
            private set 
            {
                if (!ckDPI) return;
                if (!value.HasValue || _session.State != (int) NTwain.State.SourceOpened) return;
                _session.CurrentSource.Capabilities.ICapXResolution.SetValue(value.Value);
                _session.CurrentSource.Capabilities.ICapYResolution.SetValue(value.Value);
            }
        }

        public PixelType? PixelType
        {
            get
            {
                if (!ckPixelType || _session.State != (int)NTwain.State.SourceOpened)
                    return null;
                return _session.CurrentSource.Capabilities.ICapPixelType.GetCurrent();
            }
            private set
            {
                if (!ckPixelType) return;
                if (!value.HasValue || _session.State != (int)NTwain.State.SourceOpened) return;
                _session.CurrentSource.Capabilities.ICapPixelType.SetValue(value.Value);
            }
        }

        public BoolType? Duplex
        {
            get
            {
                if (!ckDuplex || _session.State != (int)NTwain.State.SourceOpened)
                    return null;
                return _session.CurrentSource.Capabilities.CapDuplexEnabled.GetCurrent();
            }
            private set
            {
                if (!ckDuplex) return;
                if (!value.HasValue || _session.State != (int)NTwain.State.SourceOpened) return;
                _session.CurrentSource.Capabilities.CapDuplexEnabled.SetValue(value.Value);
            }
        }

        public SupportedSize? Size
        {
            get
            {
                if (!ckSize || _session.State != (int)NTwain.State.SourceOpened)
                    return null;
                return _session.CurrentSource.Capabilities.ICapSupportedSizes.GetCurrent();
            }
            private set
            {
                if (!ckSize) return;
                if (!value.HasValue || _session.State != (int)NTwain.State.SourceOpened) return;
                _session.CurrentSource.Capabilities.ICapSupportedSizes.SetValue(value.Value);
            }
        }

        public TwainSimple()
        {
            var id = TWIdentity.CreateFromAssembly(DataGroups.Image, Assembly.GetEntryAssembly());
            _session = new TwainSession(id) {SynchronizationContext = SynchronizationContext.Current};
            SignOnParentEvents();
        }

        #region Events

        private void SignOnParentEvents()
        {
            #region Transferred Event

            _session.DataTransferred += (s, args) =>
            {
                Image img = null;
                if (args.NativeData != IntPtr.Zero)
                {
                    var stream = args.GetNativeImageStream();
                    if (stream != null) img = Image.FromStream(stream);
                }
                else if (!string.IsNullOrEmpty(args.FileDataPath)) img = new Bitmap(args.FileDataPath);
                else if (args.MemoryData != null && args.MemoryData.Length > 0)
                {
                    var info = args.ImageInfo;

                    MainForm.Logger.Warn("MemoryTransfer режим передачи на " + _session.CurrentSource.Name);
                    MainForm.Logger.Info("bitpPixel {0}, length {1}, width {2}, pixelType {3}, Resolution {4}",
                        info.BitsPerPixel, info.ImageLength, info.ImageWidth, info.PixelType, info.XResolution);

                    using (
                        var bmp = new Bitmap(info.ImageWidth, info.ImageLength,
                            info.BitsPerPixel == 1
                                ? PixelFormat.Format1bppIndexed
                                : PixelFormat.Format24bppRgb))
                    {
                        var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);
                        Marshal.Copy(args.MemoryData, 0, bmpData.Scan0, args.MemoryData.Length);
                        bmp.UnlockBits(bmpData);
                        img = bmp.Clone() as Image;
                    }
                }
                if (img == null)
                {
                    MainForm.Logger.Warn("Отсутствует передаваемое изображение");
                    return;
                }
                OnImageTransferred(img);
            };
            #endregion
            #region Other event

            _session.StateChanged += (s, e) =>
            {
                MainForm.Logger.Info("State changed to {0} on thread {1}", _session.State, Thread.CurrentThread.ManagedThreadId);
            };
            _session.TransferError += (s, e) =>
            {
                MainForm.Logger.Info("Got xfer error on thread " + Thread.CurrentThread.ManagedThreadId);
            };

            _session.SourceDisabled += (s, e) =>
            {
                MainForm.Logger.Info("Source disabled event on thread " + Thread.CurrentThread.ManagedThreadId);
            };
            _session.SourceDisabled += (s, e) =>
            {
                this.SourceDisabled.Invoke(s, e);
            };

            _session.SourceChanged += (s, e) =>
            {
                if (_session.CurrentSource == null) return;
                MainForm.Logger.Info("Открыт новый источник " + _session.CurrentSource.Name);
                UpdateValidate();
            };
            _session.SourceChanged += (s, e) =>
            {
                this.SourceChanged.Invoke(s, e);
            };

            _session.TransferReady += (s, e) =>
            {
                MainForm.Logger.Info("Transferr ready event on thread " + Thread.CurrentThread.ManagedThreadId);
            };
        #endregion
        }

        public delegate void ImageTransferredHandler(object sender, ImageTransferredEventArgs args);

        public event ImageTransferredHandler ImageTransferred;

        private void OnImageTransferred(Image image)
        {
            ImageTransferred?.Invoke(this, new ImageTransferredEventArgs(image));
        }

        #endregion
        #region Validate

        private bool ValidateUI()
        {
            var cap = _session.CurrentSource.Capabilities.CapUIControllable;
            return cap.IsSupported;
        }

        private bool ValidatePixelType()
        {
            var cap = _session.CurrentSource.Capabilities.ICapPixelType;
            if (!cap.CanGet || !cap.CanSet) return false;
            var values = cap.GetValues();
            return TwSettings.RequiredPixelTypes.All(pt => values.Contains(pt));
        }

        private bool ValidateSize()
        {
            var cap = _session.CurrentSource.Capabilities.ICapSupportedSizes;
            return cap.CanGet && cap.CanSet;
        }

        private bool ValidateDPI()
        {
            return _ValidateXResolution() && _ValidateYResolution();
        }

        private bool ValidateDuplex()
        {
            var cap = _session.CurrentSource.Capabilities.CapDuplex;
            return cap.CanGetCurrent && cap.GetCurrent() != NTwain.Data.Duplex.None;
        }

        private bool _ValidateXResolution()
        {
            var cap = _session.CurrentSource.Capabilities.ICapXResolution;
            if (!cap.CanGet || !cap.CanSet) return false;
            var values = cap.GetValues();
            return TwSettings.RequiredDpi.All(dpi => values.Contains(dpi));
        }

        private bool _ValidateYResolution()
        {
            var cap = _session.CurrentSource.Capabilities.ICapYResolution;
            if (!cap.CanGet || !cap.CanSet) return false;
            var values = cap.GetValues();
            return TwSettings.RequiredDpi.All(dpi => values.Contains(dpi));
        }

        #endregion
        #region Available options

        public TWFix32[] GetResolutions()
        {
            var cap = _session.CurrentSource.Capabilities.ICapXResolution;
            return cap.GetValues().Where(dpi => Array.Exists(TwSettings.RequiredDpi, aDpi => dpi == aDpi)).ToArray();
        }

        public SupportedSize[] GetSizes()
        {
            var cap = _session.CurrentSource.Capabilities.ICapSupportedSizes;
            var list = cap.GetValues().ToList();
            list.Sort(new PageSizeComparer());
            return list.ToArray();
        }

        #endregion

        public ReturnCode RunScanning(IntPtr handle, TWFix32? dpi, PixelType? pixel, BoolType? duplex, SupportedSize? size, bool gui)
        {
            if (_session.State != (int) NTwain.State.SourceOpened) return ReturnCode.Failure;
            try
            {
                if (!gui)
                {
                    DPI = dpi;
                    PixelType = pixel;
                    Duplex = duplex;
                    Size = size;
                    SpecScannerSettings.Apply(_session.CurrentSource.Name, _session.CurrentSource.Capabilities);

                    return _session.CurrentSource.Enable(SourceEnableMode.NoUI, false, handle);
                }
                else
                {
                    return _session.CurrentSource.Enable(SourceEnableMode.ShowUI, true, handle);
                }
            }
            catch (Exception ex)
            {
                MainForm.Logger.Error(ex, "Произошла ошибка при сканировании, сканер {0}, state {1}", _session.CurrentSource.Name, _session.State);
                throw;
            }
        }

        public void UpdateValidate()
        {
            ckUIControl = ValidateUI();
            ckDPI = ValidateDPI();
            ckPixelType = ValidatePixelType();
            ckDuplex = ValidateDuplex();
            ckSize = ValidateSize();
        }

        public void Open()
        {
            _session.Open();
        }

        public void DefaultSourceOpen()
        {
            _session.DefaultSource.Open();
        }

        public void Cleanup()
        {
            if (_session.State == 4) _session.CurrentSource.Close();
            if (_session.State == 3) _session.Close();
            if (_session.State > 2) _session.ForceStepDown(2);
        }

        public IEnumerator<DataSource> GetEnumerator()
        {
            return _session.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}