﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTwain.Data;

namespace UFRSScan
{
    internal static class TwSettings
    {
        public static readonly TWFix32[] RequiredDpi = { 200, 300, 600 };
        public static readonly PixelType[] RequiredPixelTypes = { PixelType.RGB, PixelType.BlackWhite };
    }
}
