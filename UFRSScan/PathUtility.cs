﻿using System;

namespace UFRSScan
{
    public class PathUtility
    {
        private static string _path = _GetDir();
        public static string Path
        {
            get
            {
                return string.IsNullOrEmpty(_path) ? Environment.CurrentDirectory : _path;
                
            }
            set { _path = value; }
        }

        private static string _GetDir()
        {
            //var root = System.IO.Path.GetDirectoryName(Environment.CurrentDirectory);
            return System.IO.Path.Combine(Environment.CurrentDirectory, "temp");
        }
    }
}