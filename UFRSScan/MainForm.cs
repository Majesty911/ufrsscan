﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using NTwain;
using NTwain.Data;
using NLog;
using NLog.Config;
using NLog.Targets;
using UFRSScan.Properties;


namespace UFRSScan
{
    public partial class MainForm : Form
    {
        private readonly ScannedImages _scannedImages;
        private readonly FileSystemWatcher _watcher;
        private bool _isUpdateFoder;
        private bool _loadingCaps;
        private bool _isScanning;
        private bool _isDeleting;
        private bool _needFocused = true;
        private TwainSimple _twain;
        public static Logger Logger = LogManager.GetLogger("StdLogger");

        public MainForm()
        {
            InitializeComponent();
#if (!DEBUG)
            gBoxDebug.Visible = false;
#endif
            var width = dataGridView_ScanImages.Width
                           - (dataGridView_ScanImages.Margin.Left + dataGridView_ScanImages.Margin.Right)
                           - SystemInformation.VerticalScrollBarWidth;

            _scannedImages = new ScannedImages() { ThumbnailWidth = width };
            _scannedImages.IsUpdateFolder += (s, b) => { this._isUpdateFoder = b.State; };

            _watcher = new FileSystemWatcher(PathUtility.Path, "*.bmp")
            {
                NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName
            };

            _watcher.Created += OnFileSystemChanged;
            _watcher.Deleted += OnFileSystemChanged;
            var config = new LoggingConfiguration();
            var fileTarget = new FileTarget
            {
                FileName = "${basedir}/log.txt",
                Layout = @"${date} ${level}: ${message} ${exception:format=ToString}"
            };
            config.AddTarget("file", fileTarget);
#if DEBUG
            var rule2 = new LoggingRule("*", LogLevel.Info, fileTarget);
#else
            var rule2 = new LoggingRule("*", LogLevel.Warn, fileTarget);
#endif
            config.LoggingRules.Add(rule2);
            LogManager.Configuration = config;

#region Global Hotkeys Register

            GlobalHotkeys.Register(this.Handle, GlobalHotkeys.KeyModifier.None, Keys.Add);
            _hotkeyHandle += () =>
            {
                _needFocused = false;
                View_ToolStripMenuItem_runScaning_Click(this, new EventArgs());
            };

#endregion
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dataGridView_ScanImages.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView_ScanImages.DataSource = new BindingSource(_scannedImages, "Images");
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _scannedImages.UpdateFromFolder();
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            SetupTwain();
            _watcher.EnableRaisingEvents = true;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (_twain != null)
            {
                if (e.CloseReason == CloseReason.UserClosing && _twain.State > 4) e.Cancel = true;
                else _twain.Cleanup();
            }
            Settings.Default.Save();
            GlobalHotkeys.Unregister(this.Handle);
            base.OnFormClosing(e);
        }

        private void SetupTwain()
        {
            _twain = new TwainSimple();
            _twain.ImageTransferred += (s, e) =>
            {
                this.BeginInvoke(new Action(() =>
                {
                    _scannedImages.AddImage(e.ImageData);
                }));
            };

            _twain.SourceDisabled += (s, e) =>
            {
                BeginInvoke(new Action(() =>
                {
                    View_ToolStripMenuItem_runScaning.Enabled = true;
                    SetPictureBoxAsync();
                    if (_needFocused)
                        UFRSScan.Focus.BringToFront(this.Handle);
                    _needFocused = true;
                    _isScanning = false;
                }));
            };

            _twain.SourceChanged += (s, e) =>
            {
                if (_twain.State != 4) return;
                BeginInvoke(new Action(LoadSourceCaps));
            };

            Logger.Info("Setup thread = " + Thread.CurrentThread.ManagedThreadId);
            if (_twain.State < 3)
            {
                _twain.Open();
            }

            var lastDevice = _twain.FirstOrDefault(device => device.Name == Settings.Default.device);
            if (lastDevice != null)
            {
                lastDevice.Open();
            }
            else
            {
                _twain.DefaultSourceOpen();
            }
        }

        private void ReloadSourceList()
        {
            if (_twain.State < 3) return;
            var menu = ToolStripMenuItemSettings.DropDownItems;
            while (menu.IndexOf(sepSourceList) > 0)
            {
                var first = menu[0];
                first.Click -= srcBtn_Click;
                menu.Remove(first);
            }
            foreach (var src in _twain)
            {
                if (_twain.State > 4) return;
                var srcBtn = new ToolStripMenuItem(src.Name)
                {
                    Tag = src,
                    Checked = _twain.CurrentSource != null && _twain.CurrentSource.Name == src.Name
                };
                srcBtn.Click += srcBtn_Click;
                menu.Insert(0, srcBtn);
            }
        }

        private async void SetPictureBoxAsync()
        {
            CleanupPictureBox();
            try
            {
                var image = _scannedImages.GetLastImageAsync();
                View_pictureBox_CurrentImg.Image = await image;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void srcBtn_Click(object sender, EventArgs e)
        {
            var btn = sender as ToolStripMenuItem;
            var btnScan = btn?.Tag as DataSource;
            if (btn == null) return;
            if (_twain.State > 4) return;
            if (_twain.CurrentSource == btnScan) return;
            if (!_twain.Contains(btnScan)) return;

            if (_twain.CurrentSource.Close() != ReturnCode.Success) return;
            foreach (var bt in btn.GetCurrentParent().Items.OfType<ToolStripMenuItem>())
            {
                bt.Checked = false;
            }

            if (btnScan.Open() != ReturnCode.Success) return;
            btn.Checked = true;
            Settings.Default.device = btnScan.Name;
        }

        private void LoadSourceCaps()
        {
            _loadingCaps = true;
            checkBox_HideInterface.Enabled = _twain.ckUIControl;
            checkBox_HideInterface.Checked = _twain.ckUIControl;

            if (groupBox_DPI.Enabled = _twain.ckDPI) LoadDPI();

            if (groupBox_Format.Enabled = _twain.ckSize) LoadPaperSize();

            if (groupBox_Duplex.Enabled = _twain.ckDuplex)
            {
                LoadDuplex();
            }
            else
            {
                foreach (var btn in groupBox_Duplex.Controls.OfType<RadioButton>())
                {
                    btn.Checked = false;
                }
            }

            if (View_groupBox_color.Enabled = _twain.ckPixelType)
            {
                LoadPixelType();
            }
            else
            {
                View_groupBox_color.Controls.OfType<RadioButton>().Select(button => button.Checked = false);
            }

            _loadingCaps = false;
        }

        private void LoadDuplex()
        {
            radioButton_Duplex_on.Checked = _twain.Duplex == BoolType.True;
            radioButton_Duplex_off.Checked = _twain.Duplex == BoolType.False;
            radioButton_Duplex_on.Tag = BoolType.True;
            radioButton_Duplex_off.Tag = BoolType.False;
        }

        private void LoadPixelType()
        {
            view_radioButton_BW.Checked = _twain.PixelType == PixelType.BlackWhite;
            view_radioButton_color.Checked = _twain.PixelType == PixelType.RGB;
            view_radioButton_BW.Tag = PixelType.BlackWhite;
            view_radioButton_color.Tag = PixelType.RGB;
        }

        private void LoadDPI()
        {
            if (!_loadingCaps || _twain.State != 4) return;
            var list = _twain.GetResolutions();
            TWFix32 settingDpi = list.FirstOrDefault(dpi => dpi == Settings.Default.dpi);
            var cur = settingDpi != 0
                ? settingDpi
                : _twain.DPI.Value;
            View_comboBox_DPI.DataSource = list;
            View_comboBox_DPI.SelectedItem = cur;
        }

        private void LoadPaperSize()
        {
            if (!_loadingCaps || _twain.State != 4) return;
            var list = _twain.GetSizes();
            comboBox_Format.DataSource = list;
            comboBox_Format.SelectedItem = _twain.Size.Value;
        }

        private void View_ToolStripMenuItem_runScaning_Click(object sender, EventArgs e)
        {
            if (_twain.State != 4) return;
            try
            {
                var dpi = View_comboBox_DPI.SelectedItem as TWFix32?;
                var pixel = View_groupBox_color.Controls.
                        Cast<RadioButton>().FirstOrDefault(btn => btn.Checked)?.Tag as PixelType?;
                var duplex = View_groupBox_color.Controls.
                        Cast<RadioButton>().FirstOrDefault(btn => btn.Checked)?.Tag as BoolType?;
                var size = comboBox_Format.SelectedItem as SupportedSize?;
                var showUI = !checkBox_HideInterface.Checked;
#region DEBUG
#if DEBUG
                var src = _twain.CurrentSource.Capabilities;
                src.ICapXferMech.SetValue(cBoxNative.Checked ? XferMech.Native : src.ICapXferMech.GetDefault());
                if (src.CapAutoFeed.CanSet)
                {
                    src.CapAutoFeed.SetValue(cBoxAutoFeed.Checked ? BoolType.True : BoolType.False);
                }
                if(src.CapAutomaticSenseMedium.CanSet)
                    src.CapAutomaticSenseMedium.SetValue(cBoxAutoFeed.Checked ? BoolType.True : BoolType.False);
                if (src.ICapAutomaticBorderDetection.CanSet)
                    src.ICapAutomaticBorderDetection.SetValue(cBoxAutoBorder.Checked ? BoolType.True : BoolType.False);
                if (src.ICapAutomaticDeskew.CanSet)
                {
                    src.ICapAutomaticDeskew.SetValue(cBoxDeskew.Checked ? BoolType.True : BoolType.False);
                }
#endif
#endregion
                if (_twain.RunScanning(this.Handle, dpi, pixel, duplex, size, showUI) == ReturnCode.Success)
                {
                    _isScanning = true;
                    View_ToolStripMenuItem_runScaning.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView_ScanImages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView_ScanImages_SelectionChanged(sender, EventArgs.Empty);
        }

        private async void dataGridView_ScanImages_SelectionChanged(object sender, EventArgs e)
        {
            var dgv = (DataGridView)sender;
            CleanupPictureBox();
            if (dgv.CurrentRow == null) return;
            if (_isUpdateFoder) return;
            var scanImage = (ScannedImage) dgv.CurrentRow.DataBoundItem;

            try
            {
                using (var image = scanImage.GetImageAsync())
                {
                    View_pictureBox_CurrentImg.Image = await image;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CleanupPictureBox()
        {
            if (View_pictureBox_CurrentImg.Image == null) return;
            View_pictureBox_CurrentImg.Image.Dispose();
            View_pictureBox_CurrentImg.Image = null;
        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (ToolStripMenuItemSettings.DropDownItems.Count == 2)
            {
                ReloadSourceList();
            }
        }

        private void toolStripMenuItemReload_Click(object sender, EventArgs e)
        {
            ReloadSourceList();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    View_ToolStripMenuItem_runScaning_Click(this, new EventArgs());
                    break;
                case Keys.Delete:
                    DeleteCurrentImage();
                    break;
            }
        }

        private void DeleteCurrentImage()
        {
            var dgv = dataGridView_ScanImages;
            if (dgv.CurrentRow == null) return;
            if (_isUpdateFoder) return;
            var scanImage = (ScannedImage)dgv.CurrentRow.DataBoundItem;
            _isDeleting = true;
            try
            {
                _scannedImages.RemoveImage(scanImage);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally { _isDeleting = false; }
        }

        private void OnFileSystemChanged(object sender, FileSystemEventArgs args)
        {
            if (_isScanning || _isDeleting || _isUpdateFoder) return;
            this.BeginInvoke(new Action(() =>
            {
                _scannedImages.UpdateFromFolder();
            }));
        }

        private void View_comboBox_DPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            var btn = sender as ComboBox;
            var dpi = btn?.SelectedItem as TWFix32?;
            if (dpi.HasValue)
                Settings.Default.dpi = (int) dpi.Value;
        }
    }
}