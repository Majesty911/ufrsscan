﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace UFRSScan
{
    public class ScannedImages
    {
        private readonly BindingList<ScannedImage> _images = new BindingList<ScannedImage>();
        public BindingList<ScannedImage> Images => _images;
        public int Counts { get; private set; }
        public int ThumbnailWidth { get; set; }

        public void AddImage(Image image)
        {
            var fileName = Counts.ToString("D4");
            var pathToImg = Path.Combine(PathUtility.Path, Path.ChangeExtension(fileName, "bmp"));
            try
            {
                //using (var destinationStream = File.Create(pathToImg))
                //{
                //    image.Save(destinationStream, ImageFormat.Bmp);
                //}
                image.Save(pathToImg);
                _AddImage(image, pathToImg);
            }
            catch (Exception ex) { throw new Exception(string.Format("Не удалось сохранить изображение в {0} : {1}", pathToImg, ex.Message), ex); }
        }

        //public Image GetLastImage()
        //{
        //    var image = _images.Count > 0 ? _images.Last() : null;
        //    return image?.GetImage();
        //}

        public Task<Bitmap> GetLastImageAsync()
        {
            var image = _images.Count > 0 ? _images.Last() : null;
            return image?.GetImageAsync();
        }

        public bool UpdateFromFolder()
        {
            var isUpdate = _images.Where(img => !File.Exists(img.PathToImg))
                .Select(_DelImage)
                .Any(x => x);
            var files = Directory.GetFiles(PathUtility.Path, "*.bmp", SearchOption.TopDirectoryOnly)
                .Except(_images.Select(img => img.PathToImg))
                .ToList();

            if (!files.Any()) return isUpdate;
            OnUpdateFolder();
            try
            {
                var parseCounts = new List<int>();
                foreach (var file in files)
                {
                    int parseCount;
                    Int32.TryParse(Path.GetFileNameWithoutExtension(file), out parseCount);
                    parseCounts.Add(parseCount);
                    _AddImageByFile(file);
                    isUpdate = true;
                }
                var maxParseCount = parseCounts.Max();
                if (Counts <= maxParseCount) Counts = maxParseCount + 1;
            }
            finally { OffUpdateFolder(); }

            return isUpdate;
        }

        public void RemoveImage(ScannedImage scannedImage)
        {
            try
            {
                File.Delete(scannedImage.PathToImg);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Не удается удалить файл {0} : {1}", scannedImage.PathToImg, ex.Message), ex);
            }
            _images.Remove(scannedImage);
            if (_images.Count == 0) Counts = 0;
        }

        private void _AddImage(Image image, string pathToImg)
        {
            var icon = GetThumbnail(image);
            _images.Add(new ScannedImage(icon, Counts, pathToImg));
            Counts++;
        }

        private bool _DelImage(ScannedImage image)
        {
            _images.Remove(image);
            if (_images.Count == 0) Counts = 0;
            return true;
        }

        private void _AddImageByFile(string pathToImg)
        {
            try
            {
                using (var readStream = new FileStream(pathToImg, FileMode.Open))
                {
                    Image image = new Bitmap(readStream);
                    _AddImage(image, pathToImg);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Не удалось прочитать файл {0} : {1}", pathToImg, ex.Message), ex);
            }
        }

        public delegate void UpdateFolderHandler(object sender, IsUpdateFolderArgs args);

        public event UpdateFolderHandler IsUpdateFolder;

        protected virtual void OnUpdateFolder()
        {
            IsUpdateFolder?.Invoke(this, new IsUpdateFolderArgs(true));
        }

        protected virtual void OffUpdateFolder()
        {
            IsUpdateFolder?.Invoke(this, new IsUpdateFolderArgs(false));
        }

        private Image GetThumbnail(Image image)
        {
            var newHeight = image.Height * ThumbnailWidth / image.Width;
            return image.GetThumbnailImage(ThumbnailWidth, newHeight, null, IntPtr.Zero);
        }
    }


    public struct IsUpdateFolderArgs
    {
        public bool State { get; set; }
        public IsUpdateFolderArgs(bool state)
        {
            State = state;
        }
    }
}