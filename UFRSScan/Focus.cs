﻿using System;
using System.Runtime.InteropServices;

namespace UFRSScan
{
    public static class Focus
    {
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void BringToFront(IntPtr handle)
        {
            SetForegroundWindow(handle);
        }
    }
}
