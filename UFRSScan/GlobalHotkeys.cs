﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace UFRSScan
{
    internal static class GlobalHotkeys
    {
        private const int Id = 0;

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }

        public static void Register(IntPtr handle, KeyModifier modifier, Keys key)
        {
            RegisterHotKey(handle, Id, (int) modifier, key.GetHashCode());
        }

        public static void Unregister(IntPtr handle)
        {
            UnregisterHotKey(handle, 0);
        }
    }


    public partial class MainForm
    {
        private delegate void HotkeyHandle();

        private readonly HotkeyHandle _hotkeyHandle;

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                //Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);
                //GlobalHotkeys.KeyModifier modifier = (GlobalHotkeys.KeyModifier)((int)m.LParam & 0xFFFF);
                //int id = m.WParam.ToInt32();
                _hotkeyHandle.Invoke();
            }
        }
    }
}
